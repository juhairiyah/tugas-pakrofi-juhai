import React, {Component} from 'react';
import {StyleSheet, ScrollView, View, Text, Image, TouchableOpacity } from 'react-native';

class FlexBox extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.aflex}>
                    <Image source={require('../assets/a1.jpg')} style={{width:150,height:60}}/>
                    <Image source={require('../assets/a2.jpg')} style={{width:50,height:50, marginLeft:180}}/>
                    <Image source={require('../assets/a3.jpg')} style={{width:50,height:50}}/>
                    <Text style={styles.position}>15</Text>
                </View>
                <View style={styles.bflex}>
                    <ScrollView>
                        <ScrollView horizontal>
                            <TouchableOpacity>
                            <Image source={require('../assets/a4.jpg')} style={styles.cerita}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('../assets/a4.jpg')} style={styles.cerita}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('../assets/a4.jpg')} style={styles.cerita}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('../assets/a4.jpg')} style={styles.cerita}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('../assets/a4.jpg')} style={styles.cerita}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('../assets/a4.jpg')} style={styles.cerita}/></TouchableOpacity>
                        </ScrollView>
                        <Image source={require('../assets/a5.jpg')} style={styles.utama}/>
                        <Image source={require('../assets/a5.jpg')} style={styles.utama}/>
                        <Image source={require('../assets/a5.jpg')} style={styles.utama}/>
                        <Image source={require('../assets/a5.jpg')} style={styles.utama}/>
                        <Image source={require('../assets/a5.jpg')} style={styles.utama}/>
                        <Image source={require('../assets/a5.jpg')} style={styles.utama}/>
                    </ScrollView>
                </View>
                <View style={styles.cflex}>
                    <Image source={require('../assets/a6.jpg')} style={styles.footer}/>
                    <Image source={require('../assets/a7.jpg')} style={styles.footer}/>
                    <Image source={require('../assets/a8.jpg')} style={styles.footer}/>
                    <Image source={require('../assets/a9.jpg')} style={styles.footer}/>
                    <Image source={require('../assets/a10.jpg')} style={styles.footer}/>
                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container: {
        flex:1
    },
    aflex:{
        flex:1,
        flexDirection:'row',
        backgroundColor:'white',
        alignContent:'center',
        alignItems:'center'
    },
    bflex:{
        flex:10,
        backgroundColor:'white'
    },
    cflex:{
        flex:1,
        flexDirection:'row',
        backgroundColor:'white',
        alignItems:'center',
        alignContent:'center',
        justifyContent:'space-around'
    },
    cerita:{
        width:90,
        height:90,
        borderRadius:50,
        borderWidth:2,
        borderColor:'red',
        marginLeft:10
    },
    utama:{
        width:425,
        height:670
    },
    footer:{
        width:50,
        height:50
    },
    position:{
        fontSize:13,
        color:'white',
        backgroundColor:'red',
        width:24,
        height:24,
        borderRadius:12,
        padding:6,
        position:'absolute',
        top:4,
        right:0
    }
});

export default FlexBox;